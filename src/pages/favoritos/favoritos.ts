import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FavoriteProvider } from '../../providers/favorite/favorite';
import { VisualizarPage } from '../visualizar/visualizar';

@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html'
})
export class FavoritosPage {
  results: any[] = [];
  searchService: string;
  constructor(public navCtrl: NavController, public favoriteProvider: FavoriteProvider) {
  }

  ionViewDidLoad() {
    this.getAllFavorites();
  }
  
  public getAllFavorites() {
    this.favoriteProvider.getAll()
      .then((result: any[]) => {
        this.results = result;
      });
  }

  public filterItems() {
    console.log(this.searchService);
  }

  public open(id) {

    this.navCtrl.push(VisualizarPage, { 'id': id });
  }

}
