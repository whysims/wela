import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Users } from '../../model/users';
import { RestApiProvider } from '../../providers/rest-api/rest-api';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public user = {};
  public success: Boolean = false;
  public successMessage: string;

  constructor(public navCtrl: NavController, public toastCtrl: ToastController,public navParams: NavParams, public restApi: RestApiProvider) {
  }

  ionViewDidLoad() {
  }

  registerUser(): void {
    this.restApi.createUser(this.user).subscribe(result => {
      if (result['success']) {
        this.success = true;
      } else {
        this.success = false;
        this.toastCtrl.create({message: result['message'],duration: 3000, position: 'bottom'}).present();
      }
    });
  }

}
