import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CentralMessagesPage } from './central-messages';

@NgModule({
  declarations: [
    CentralMessagesPage,
  ],
  imports: [
    IonicPageModule.forChild(CentralMessagesPage),
  ],
})
export class CentralMessagesPageModule {}
