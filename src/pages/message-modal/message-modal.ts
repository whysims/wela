import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { MessagePage } from '../message/message';

/**
 * Generated class for the MessageModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-message-modal',
  templateUrl: 'message-modal.html',
})
export class MessageModalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    
  }

  public sendMessage(type) {
    this.navCtrl.push(MessagePage);
  }

  private closeMessageModal() {
    this.viewCtrl.dismiss();
  }

}
