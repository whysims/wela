import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { RestApiProvider } from '../../providers/rest-api/rest-api';
import { WelaPage } from '../wela/wela';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public user = {};
  public logged: Boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public restApi: RestApiProvider) {
    if (sessionStorage.getItem('user')) {
      this.navCtrl.popTo(WelaPage);
    }
  }

  ionViewDidLoad() {
  }
  login() {
    
    this.restApi.userLogin(this.user).subscribe(result => {
      if (result['success']) {
        this.logged = true;
        sessionStorage.setItem('user', JSON.stringify(result['message']));
        this.navCtrl.popTo(WelaPage);
      } else {
        this.toastCtrl.create({message: result['message'], duration: 5000, position: 'bottom'}).present();
      }
    });
  }
  signup() {
    this.navCtrl.push(RegisterPage);
  }
}
