import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-conversar',
  templateUrl: 'conversar.html'
})
export class ConversarPage {

  constructor(public navCtrl: NavController) {
  }
  
}
