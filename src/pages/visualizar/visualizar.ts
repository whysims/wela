import { Component } from '@angular/core';
import { MessageModalPage } from '../message-modal/message-modal';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { RestApiProvider } from '../../providers/rest-api/rest-api';
import { CardModel } from '../../model/card';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FavoriteProvider } from '../../providers/favorite/favorite';

import { ToastController } from 'ionic-angular';


@Component({
  selector: 'page-visualizar',
  templateUrl: 'visualizar.html'
})
export class VisualizarPage {

  showComment = false;
  showDescription = false;
  cardId = 0;
  cardData: CardModel;
  cardPhoto: any;
  errorMessage: string;

  constructor(public navCtrl: NavController, private toastCtrl: ToastController, public favoriteProvider: FavoriteProvider, public navParams: NavParams, public rest: RestApiProvider, public share: SocialSharing) {
    this.cardId = navParams.get('id');

  }

  ionViewDidLoad() {
    this.getData();
  }
  saveFavorite() {
    let heartanimationfour = document.getElementById('heart4');
  
    this.favoriteProvider.insert(this.cardData, this.cardPhoto)
      .then(() => {
        this.toastCtrl.create({message: 'Cartão favoritado com sucesso!', duration: 3000, position: 'bottom'}).present();
      })
      .catch((e) => { console.error("Erro ao criar o cartão"); });
  }
  toggleComments(): void {
    this.showComment = !this.showComment;
  }
  toggleDescription(): void {
    this.showDescription = !this.showDescription;
  }
  sendAMessage(): void {
    this.navCtrl.push(MessageModalPage);
  }
  callShare() {
    let msg = this.cardData.title + "\n" + this.cardData.author + "\nTelefone: " + this.cardData.phone + "\nInformações retiradas do aplicativo Wela.";
    this.share.share(msg, null, 'http://otavioalenski.techpublic.com.br/wela/api/v2/cardsphoto/' + this.cardPhoto.photo_url, null);
  }
  getData() {
    this.rest.retrieveCard(this.cardId).subscribe(
      data => {
        this.cardData = data['card']['card_data'];
        this.cardPhoto = data['card']['card_photo']
      },
      error => this.errorMessage = <any>error
    );
  }

}
