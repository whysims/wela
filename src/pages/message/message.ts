import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the MessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {
  messages = [];
  nickname = '' || 'Visita';
  message = '';
  socket: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.socket = io('https://welaapp.herokuapp.com:13918');

  }

  ionViewDidLoad() {

  }
  send(msg) {
    if(msg!='') {
     // Assign user typed message along with generated user id
     let saltedMsg =  "Visita #" + msg; 
     // Push the message through socket 
     this.socket.emit('message', saltedMsg);
    }
    this.message='';
  }
  Receive(){
    // Socket receiving method 
    this.socket.on('message', (msg) => {
      // separate the salted message with "#" tag 
      let saletdMsgArr = msg.split('#');
      var item= {};
      // check the sender id and change the style class
      if(saletdMsgArr[0] == 'Visita'){
         item = { "styleClass":"chat-message right", "msgStr":saletdMsgArr[1] };
      }
      else{
         item= { "styleClass":"chat-message left", "msgStr":saletdMsgArr[1] };
      }
      // push the bind object to array
      // Final chats array will iterate in the view  
      this.messages.push(item);
    });
 }
  ionViewWillLeave() {
  }

}
