import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PesquisaPage } from '../pesquisa/pesquisa';


@Component({
  selector: 'page-wela',
  templateUrl: 'wela.html'
})
export class WelaPage {
  public logged;
  
  constructor(public navCtrl: NavController) {
    if (sessionStorage.getItem('user')) {
      this.logged = JSON.parse(sessionStorage.getItem('user'));
    } else {
      this.logged = false;
    }
    
  }

  searchContent(value) {
    // console.log(value);
    this.navCtrl.setRoot(PesquisaPage, { search: value });
  }
}
