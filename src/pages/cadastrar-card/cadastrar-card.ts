import { Component } from '@angular/core';
import {  NavController, NavParams, LoadingController } from 'ionic-angular';

import { LottieAnimationViewModule } from 'ng-lottie';
import { ViewController } from 'ionic-angular';
import { RestApiProvider } from '../../providers/rest-api/rest-api';
import { FileUploadOptions, FileTransfer } from '@ionic-native/file-transfer';
import { Camera } from '@ionic-native/camera';
import { normalizeURL } from 'ionic-angular';
import { ConnectionCheckProvider } from '../../providers/connection-check/connection-check';

import { ToastController } from 'ionic-angular';

@Component({
  selector: 'cadastrar-card',
  templateUrl: 'cadastrar-card.html'
})
export class CadastrarCardPage {
  public lottieConfig: Object;
  showSuccess = false;
  public card = {};
  public imageMessage: any;
  public imagePreview: any;
  public imageFile: any;
  public imageTransfer: any;
  public loader = this.loadingCtrl.create({content: "Enviando Imagem..."});

  constructor(public navCtrl: NavController,public toastCtrl: ToastController, public checkConn: ConnectionCheckProvider ,private transfer: FileTransfer, private camera: Camera, public loadingCtrl: LoadingController, public navParams: NavParams, public viewCtrl: ViewController, public restApi: RestApiProvider) {
    LottieAnimationViewModule.forRoot();
    this.lottieConfig = {
      path: 'assets/animation/success.json',
      autoplay: true,
      loop: false
    };
  }

  ionViewDidLoad() {
  }

  getImage() {
    
    let options = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    };

    this.camera.getPicture(options).then((imageData) => {
      this.imagePreview = imageData;
      this.imageFile = normalizeURL(imageData);
      this.imageTransfer = this.transfer.create();
    })
  }

  public registerCard(): void {
    this.loader.present();
    this.checkConn.onlineCheck().then(
      () => {

    this.restApi.createCard(this.card).subscribe(
      data => {
        if (data['success']) {
          this.showSuccess = !this.showSuccess;
          this.uploadFile(data['data']['id']);
        } else {
          this.loader.dismiss();
          alert('Erro ao salvar seu cartão.' + data['message']);
        }
      }
    )
  }).catch(
    () => {
      this.toastCtrl.create({message: "Parece que você está sem conexão com a internet.", duration: 3000, position: 'bottom'}).present()
    });
  }

  public uploadFile(id) {
    let title = this.card['title'].replace(/\s/g, "_");
    let options1: FileUploadOptions = {
      fileKey: 'fileToUpload',
      fileName: title + ".jpeg",
      params: { "card_id": id },
      mimeType: "multipart/form-data",
      headers: {} }

    this.imageTransfer.upload(this.imageFile, 'http://otavioalenski.techpublic.com.br/wela/api/v2/cardsphoto/create.php', options1)
      .then((data) => {
        this.loader.dismiss();
      }, (err) => {
        // error
        alert("error" + err.message);
      });
  }

}
