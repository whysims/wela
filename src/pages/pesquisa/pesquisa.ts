import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { VisualizarPage } from '../visualizar/visualizar';
import { SearchPipe } from '../../pipes/search/search';
import { SortPipe } from '../../pipes/sort/sort';
import { RestApiProvider } from '../../providers/rest-api/rest-api';
import * as $ from 'jquery';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-pesquisa',
  templateUrl: 'pesquisa.html'
})
export class PesquisaPage {
  cards: Observable<string[]>;
  errorMessage: string;
  descending: boolean = false;
  order: number;
  column: string = 'category';

  constructor(public navCtrl: NavController, public navParams: NavParams, public rest: RestApiProvider) {
    let searchValue = navParams.get('search');
  }

  ngAfterViewInit() {
    let items = $(".wela__search-card");
    let time = 300;
    let timeUnit = 'ms';

    items.each(function(index) {
      $(this).css({
        'animation-delay': (1+index) * time + timeUnit
      })
    });

  }

  ionViewDidLoad() {
    this.retrieveCards();
  }
  sort(){
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }

  retrieveCards() {
    this.rest.retrieveListCards().subscribe(
      cards => this.cards = cards['data'],
      error => this.errorMessage = <any>error
    );
    console.log(this.cards);
  }

  openCard(event) {
    var target = event;
    this.navCtrl.push(VisualizarPage, { 'id': target });
  }


}
