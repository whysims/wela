import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { WelaPage } from '../pages/wela/wela';
import { CadastrarCardPage } from '../pages/cadastrar-card/cadastrar-card';
import { CentralMessagesPage } from '../pages/central-messages/central-messages';
import { LoginPage } from '../pages/login/login';
import { FavoritosPage } from '../pages/favoritos/favoritos';

import { DatabaseProvider } from '../providers/database/database';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = WelaPage;

  constructor(public platform: Platform,public statusBar: StatusBar, dbProvider: DatabaseProvider) {
    dbProvider.createDatabase().catch(e => console.log(e));
    
  }


  registerCard() {
    this.nav.push(CadastrarCardPage);
  }

  centralMessage() {
    this.nav.push(CentralMessagesPage);
  }

  checkLogin() {
    this.nav.push(LoginPage);
  }

  showFavorites() {
    this.nav.push(FavoritosPage);
  }

}
