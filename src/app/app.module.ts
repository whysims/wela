import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { WelaPage } from '../pages/wela/wela';
import { PesquisaPage } from '../pages/pesquisa/pesquisa';
import { FavoritosPage } from '../pages/favoritos/favoritos';
import { ConversarPage } from '../pages/conversar/conversar';
import { VisualizarPage } from '../pages/visualizar/visualizar';
import { MessageModalPage } from '../pages/message-modal/message-modal';
import { SearchPipe } from '../pipes/search/search';
import { SortPipe } from '../pipes/sort/sort';
import { CadastrarCardPage } from '../pages/cadastrar-card/cadastrar-card';
import { CadastrarUsuarioPage } from '../pages/cadastrar-usuario/cadastrar-usuario';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

import { LottieAnimationViewModule } from 'ng-lottie';

import { StatusBar } from '@ionic-native/status-bar';
import { RestApiProvider } from '../providers/rest-api/rest-api';
import { CentralMessagesPage } from '../pages/central-messages/central-messages';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { MessagePage } from '../pages/message/message';

import { SocialSharing } from '@ionic-native/social-sharing';
import { SQLite } from '@ionic-native/sqlite'

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { Toast } from '@ionic-native/toast';
import { DatabaseProvider } from '../providers/database/database';
import { FavoriteProvider } from '../providers/favorite/favorite';
import { ConnectionCheckProvider } from '../providers/connection-check/connection-check';
const config: SocketIoConfig = { url: 'https://welaapp.herokuapp.com:19820/', options: {} };


@NgModule({
  declarations: [
    MyApp,
    WelaPage,
    PesquisaPage,
    FavoritosPage,
    ConversarPage,
    VisualizarPage,
    MessageModalPage,
    MessagePage,
    SearchPipe,
    SortPipe,
    CadastrarCardPage,
    CadastrarUsuarioPage,
    CentralMessagesPage,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule, HttpModule,
    LottieAnimationViewModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    LottieAnimationViewModule,
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelaPage,
    PesquisaPage,
    FavoritosPage,
    ConversarPage,
    VisualizarPage,
    MessageModalPage,
    MessagePage,
    CadastrarCardPage,
    CadastrarUsuarioPage,
    CentralMessagesPage,
    LoginPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    RestApiProvider,
    SocialSharing,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Camera,
    SQLite,
    FileTransfer,  
    FileTransferObject,
    DatabaseProvider,
    FavoriteProvider,
    Toast,
    ConnectionCheckProvider
  ]
})
export class AppModule { }
