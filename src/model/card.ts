export class CardModel {
    public id: number;
    public author: string;
    public title: string;
    public category: string;
    public description: string;
    public address: string;
    public phone: string;
    public whatsapp: string;
    public website: string;
    public facebook: string;
    public instagram: string;
    public youtube: string;
    public tags: string[];
    public eco: number;
    public terms: number;
    public photo_url: string;
}