export class Users {
    public id: string;
    public name: string;
    public birthday: string;
    public email: string;
    public eula: string;
    public main_phone: number;
    public address: string;
    public city: string;
    public state: string;
    public register_date: string;
}