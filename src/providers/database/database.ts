import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  constructor(public http: HttpClient, private sqlite: SQLite) {
  }

  public getDb() {
    return this.sqlite.create({
      name: 'favorites.db',
      location: 'default'
    });
  }

  public createDatabase() {
    return this.getDb().then(
      (db: SQLiteObject) => {
        this.createTables(db);
      }
    ).catch(e => console.log(e));
  }

  private createTables(db: SQLiteObject) {

    db.sqlBatch([
      ['CREATE TABLE IF NOT EXISTS favorites (id integer primary key AUTOINCREMENT NOT NULL, card_id integer, title TEXT, description TEXT, category TEXT, author TEXT, phone integer, insert_time DATETIME, address TEXT, photo_url TEXT)']
    ]).then(() => console.log('Tabelas geradas'))
    .catch(e => console.log(e));
  }
}
