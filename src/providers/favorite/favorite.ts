import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../database/database';
import { CardModel } from '../../model/card';
/*
  Generated class for the FavoriteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FavoriteProvider {

  constructor(public http: HttpClient, private dbProvider: DatabaseProvider) {
  }

  public insert(card: CardModel, cardPhoto: any) {
    return this.dbProvider.getDb()
    .then((db: SQLiteObject) => {
      let sql = "insert into favorites (card_id, title, category, description, phone, address, author, photo_url) values (?,?,?,?,?,?,?,?)";
    
      let data = [card.id, card.title, card.category, card.description, card.phone,card.address,card.author,cardPhoto.photo_url];
      return db.executeSql(sql, data).then(() => { console.log("Wela - Card criado com sucesso") }).catch((e) => console.error(e.message));
    })
    .catch((e) => console.log(e));
  }
  public remove(id: number) {
    return this.dbProvider.getDb()
      .then((db: SQLiteObject) => {
        let sql = 'delete from favorites where id = ?';
        let data = [id];
 
        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }
  public getAll() {
    return this.dbProvider.getDb() 
      .then((db: SQLiteObject) => {
 
        return db.executeSql('select * from favorites', [])
          .then((data: any) => {

            if (data.rows.length > 0) {
              let results: any[] = [];

              for (var i = 0; i < data.rows.length; i++) {
                var result = data.rows.item(i);
                results.push(result);
              }

              return results;
            } else {
              return [];
            }
          })
          .catch((e) => console.error(e.message));
      })
      .catch((e) => console.error(e.message));
  }

}

