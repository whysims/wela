import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { CardModel } from '../../model/card';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
/*
Generated class for the RestApiProvider provider.

See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/

@Injectable()
export class RestApiProvider {
  private listApi = 'http://otavioalenski.techpublic.com.br/wela/api/retrievelist.php';
  private cardApi = "http://otavioalenski.techpublic.com.br/wela/api/v2/cards/";
  private usersApi = "http://otavioalenski.techpublic.com.br/wela/api/v2/users/";
  private cardPhoto = "";


  constructor(public httpClient: HttpClient, public http: Http) {
  }

  retrieveListCards(): Observable<string[]> {
    return this.httpClient.get(this.cardApi+"read.php").pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  createCard(cardObject) {
    return this.httpClient.post(this.cardApi+"create.php",cardObject).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  retrieveCard(id): Observable<CardModel> {
    return this.httpClient.get(this.cardApi +"open.php?id="+id).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  createPhoto(photo) {
    return this.httpClient.post(this.cardPhoto+"create.php", photo).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  createUser(user) {
    return this.httpClient.post(this.usersApi+"create.php", user).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
  userLogin(user) {
    return this.httpClient.post(this.usersApi+"login.php", user).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
