import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


/*
  Generated class for the ConnectionCheckProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConnectionCheckProvider {
  private baseUrl = 'http://www.techpublic.com.br';
  private isOnline = false;

  onlineCheck() {
      let xhr = new XMLHttpRequest();
      return new Promise((resolve, reject)=>{
          xhr.onload = () => {
              // Set online status
              this.isOnline = true;
              resolve(true);
          }; 
          xhr.onerror = () => {
              // Set online status
              this.isOnline = false;
              reject(false);
          };
          xhr.open('GET', this.baseUrl, true);
          xhr.send();
      });
  }
  constructor(public http: HttpClient) {
    
  }

}
